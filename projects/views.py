from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from projects.models import Project


class ProjectListView(ListView):
    model = Project
    template_name = "projects/project_list.html"


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/project_detail.html"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template = "projects/project_create.html"
    fields = ["name", "description", "members"]
